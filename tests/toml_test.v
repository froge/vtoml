module main

import toml

fn test_key() {
	input := '"be=es" = 1'
	output := toml.parse(input) or { panic(err) }['be=es']
	val := output.int() or { panic(err) }
	assert val == 1
}

fn test_dotted_key() {
	input := 'bees.bees = 1'
	output := toml.parse(input) or { panic(err) }['bees'].map() or { panic(err) }['bees'].int() or {
		panic(err)
	}
	assert output == 1
}

fn test_bool() {
	input := 'one = true\ntwo = false'
	output := toml.parse(input) or { panic(err) }
	one := output['one'].bool() or { panic(err) }
	two := output['two'].bool() or { panic(err) }
	assert one == true && two == false
}

fn test_int() {
	input := 'int = 01589'
	output := toml.parse(input) or { panic(err) }['int'].int() or { panic(err) }
	assert output == 1589
}

fn test_comment() {
	input := 'int = 01589 # oiaernstoieanoisetnoiarensoienrasogin'
	output := toml.parse(input) or { panic(err) }['int'].int() or { panic(err) }
	assert output == 1589
}

fn test_float() {
	input := 'float = 0.1589'
	output := toml.parse(input) or { panic(err) }['float'].f64() or { panic(err) }
	assert output == 0.1589
}

fn test_string() {
	input := 'test = "bees"'
	output := toml.parse(input) or { panic(err) }['test'].string() or { panic(err) }
	assert output == 'bees'
}

fn test_array() {
	input := 'array = ["bees",\n          "bee",\n]'
	output := toml.parse(input) or { panic(err) }['array'].array() or { panic(err) }
	mut test_arr := []string{}
	for str in output {
		temp_str := str.string() or { panic(err) }
		test_arr << temp_str
	}
	assert test_arr == ['bees', 'bee']
}

fn test_tables() {
	input := '[table]\ntest = 1'
	output := toml.parse(input) or { panic(err) }['table'].map() or { panic(err) }['test'].int() or {
		panic(err)
	}
	assert output == 1
}

fn test_inline_tables() {
	input := 'table = { x = 1,\n y = 2}'
	output := toml.parse(input) or { panic(err) }['table'].map() or { panic(err) }
	x := output['x'].int() or { panic(err) }
	y := output['y'].int() or { panic(err) }
	assert x == 1 && y == 2
}

fn test_unicode() {
	input := 'bees = "\u0061bc"'
	output := toml.parse(input) or { panic(err) }['bees'].string() or { panic(err) }
	assert output == 'abc'
}
