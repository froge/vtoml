module main

import toml
import os

fn main() {
	file := os.read_file('test.toml') ?
	bees := toml.parse(file) ?
	for bee, val in bees {
		println('$bee = $val')
	}
}
