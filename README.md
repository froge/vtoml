# vtoml
This will be a feature-complete toml parser :D

# features

- [x] Key/Value pairs
    - [x] Dotted Keys (still kinda buggy, escaped quotes don't really work)
- [ ] Strings
    - [x] Basic Strings
    - [ ] compact escape sequences (almost done)
    - [x] Multi-line basic strings
    - [x] literal strings
    - [x] multi-line literal strings
- [x] Integer
    - [x] basic integers
    - [x] signs
    - [x] hex
    - [x] oct
    - [x] bin
    - [x] underscores between digits
- [x] floats
    - [x] basic floats
    - [x] signs
    - [x] exponent
    - [ ] underscores between digits
- [x] bools
- [ ] time
- [x] arrays
- [ ] tables
    - [x] normal tables
    - [ ] inline tables
    - [ ] array of tables
- [ ] Proper handling of invalid input
