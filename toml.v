module toml

import time

pub type Value = []Value | bool | f64 | int | map[string]Value | string | time.Time

enum StringType {
	no
	str
	literal_str
	multi_str
	multi_literal_str
	unicode_short
	unicode_long
}

pub fn parse(input string) ?map[string]Value {
	mut out := map[string]Value{}
	mut arr := 0
	mut tbl := 0
	mut temptbl := map[string]Value{}
	mut tempstr2 := ''
	mut tempstr := ''
	mut escaped := false
	mut str := StringType.no
	mut nonewline := false
	mut table := ''
	mut table_reg := false
	mut unicode := ''
	for line in input.split_into_lines() {
		for char in line {
			match str {
				.no {
					if char == `"` {
						str = .str
					}
					if char == `\'` {
						str = .literal_str
					}
					if tempstr.ends_with('""') && char == `"` {
						str = .multi_str
						tempstr += '"'
						nonewline = true
						continue
					}
					if tempstr.ends_with("''") && char == `\'` {
						str = .multi_literal_str
						tempstr += "'"
						nonewline = true
						continue
					}
				}
				.str {
					if !escaped {
						if char == `"` {
							str = .no
						}
					}
				}
				.literal_str {
					if char == `\'` {
						str = .no
					}
				}
				.multi_str {
					if !tempstr.ends_with('\\""') && tempstr.ends_with('""') && char == `"` {
						str = .no
						tempstr += '"'
						continue
					}
				}
				.multi_literal_str {
					if tempstr.ends_with("''") && char == `\'` {
						str = .no
						tempstr += "'"
						continue
					}
				}
				.unicode_short {
					if unicode.len == 4 {
						str = .no
						tempstr += rune(('0x' + unicode).int()).str()
						unicode = ''
					} else {
						unicode += rune(char).str()
						continue
					}
				}
				.unicode_long {
					if unicode.len == 8 {
						str = .no
						tempstr += rune(('0x' + unicode).int()).str()
						unicode = ''
					} else {
						unicode += rune(char).str()
						continue
					}
				}
			}
			// start escape sequence
			if !escaped {
				if char == `\\` {
					escaped = true
					continue
				}
			}
			// ---------------------
			// compact escape sequences
			if escaped && (str == .str || str == .multi_str) {
				match char {
					`b` {
						tempstr += '\b'
					}
					`t` {
						tempstr += '\t'
					}
					`n` {
						tempstr += '\n'
					}
					`f` {
						tempstr += '\f'
					}
					`r` {
						tempstr += '\r'
					}
					`\\` {
						tempstr += '\\'
					}
					`u` {
						str = .unicode_short
					}
					`U` {
						str = .unicode_long
					}
					else {}
				}
			}
			// ------------------------
			if nonewline {
				if char == ` ` || char == `\t` || char == `\n` {
					continue
				} else {
					nonewline = false
				}
			}
			if str == .no {
				if char == `=` && tbl == 0 {
					// TODO: rename tempstr2 to key and tempstr to val
					tempstr2 = tempstr
					tempstr = ''
					continue
				}
				if tempstr2 != '' {
					if char == `[` {
						arr++
					}
					if char == `]` {
						arr--
					}
				}
				if tempstr2 != '' {
					if char == `{` {
						tbl++
					}
					if char == `}` {
						tbl--
					}
				}
				if char == `#` {
					break
				}
				if char == ` ` || char == `\t` {
					continue
				}
			}
			/*
			if arr > 0 || ((str == .multi_str || str == .multi_literal_str) && escaped) {
				if char == `\n` || char == `\r` {
					continue
				}
			}
			*/
			if escaped {
				escaped = false
			}
			// add char to tempstr
			tempstr += char.ascii_str()
		}
		// println('$tempstr2 = $tempstr')
		if tempstr2 == '' {
			// handle array tables
			if tempstr.starts_with('[[') {
				// ------------
				// handle tables
			} else if tempstr.starts_with('[') {
				if tempstr.ends_with(']') {
					if table == '' {
						table = tempstr[1..tempstr.len - 1]
						tempstr = ''
						table_reg = false
					} else {
						out[table] = temptbl.clone()
						temptbl = map[string]Value{}
						table = tempstr[1..tempstr.len - 1]
						tempstr = ''
						table_reg = true
					}
				} else {
					// println(tempstr)
					return error('Invalid table definition')
				}
			}
		}
		// ---------------
		// add newline if multi str bc the split lines removes the newline char
		if str == .multi_str || str == .multi_literal_str {
			if !nonewline {
				tempstr += '\n'
			}
		}
		// --------------------------------------------------------------------
		// add key/value pair to table
		if tbl == 0 && arr == 0 && str != .multi_str && str != .multi_literal_str {
			if tempstr != '' {
				if table == '' {
					if tempstr2 in out {
						return error('key defined twice: $tempstr2')
					}
					// out[tempstr2] = parse_val(tempstr) ?
					add_val(mut out, tempstr2, parse_val(tempstr) ?) ?
					tempstr2 = ''
					tempstr = ''
					continue
				} else {
					if tempstr2 in temptbl {
						return error('key defined twice: $tempstr2')
					}
					// temptbl[tempstr2] = parse_val(tempstr) ?
					add_val(mut temptbl, tempstr2, parse_val(tempstr) ?) ?
					tempstr2 = ''
					tempstr = ''
					continue
				}
			}
		}
		// --------------------------
	}
	if table != '' {
		out[table] = temptbl
	}
	dump(out)
	if arr < 0 {
		return error('Missing "["')
	} else if arr > 0 {
		return error('Missing "]"')
	}
	if tbl < 0 {
		return error('Missing "{"')
	} else if tbl > 0 {
		return error('Missing "}"')
	}
	return out
}

fn parse_val(val string) ?Value {
	mut arr := 0
	mut tbl := 0
	mut ar := []Value{}
	mut temptbl := map[string]Value{}
	mut key := ''
	mut tempstr := ''
	// println('val: $val')
	// bool
	if val == 'true' {
		return true
	} else if val == 'false' {
		return false
	}
	// multi-line string
	if val.starts_with('"""') || val.starts_with("'''") {
		return val[3..val.len - 3]
	}
	// ------------------
	// normal string
	if val.starts_with('"') || val.starts_with("'") {
		return val[1..val.len - 1]
	}
	// --------------
	// numeric values
	if val[0].is_digit() {
		if val.contains('.') {
			// float
			return val.f64()
		} else {
			// integer
			return val.int()
		}
	}
	// --------------
	// inline table
	if val.starts_with('{') {
		for char in val {
			if char == `{` {
				tbl++
				if tbl == 1 {
					continue
				}
			}
			if char == `}` {
				tbl--
			}
			if char == `=` && tbl == 1 {
				key = tempstr
				tempstr = ''
				continue
			}
			if char == `,` && tbl == 1 {
				parsed := parse_val(tempstr) ?
				temptbl[key] = parsed
				key = ''
				tempstr = ''
				continue
			}
			if tbl > 0 {
				tempstr += rune(char).str()
			}
		}
		parsed := parse_val(tempstr) ?
		temptbl[key] = parsed
		key = ''
		tempstr = ''
		return temptbl
	}
	// --------------
	// array
	if val.starts_with('[') {
		for char in val {
			if char == `[` {
				arr++
				if arr == 1 {
					continue
				}
			}
			if char == `]` {
				arr--
			}
			if arr > 0 {
				tempstr += rune(char).str()
			} else {
				split := tempstr.split(',')
				for v in split {
					if v.len == 0 {
						continue
					}
					parsed := parse_val(v) ?
					ar << parsed
				}
			}
		}
		return ar
	}
	// ------
	return error('Invalid value: $val')
}

fn add_val(mut m map[string]Value, key string, value Value) ? {
	if key.contains('.') {
		mut str := false
		mut lit_str := false
		mut done := false
		mut tempstr := ''
		for index, char in key {
			match char {
				`.` {
					if !str && !lit_str {
						dump(tempstr)
						if tempstr in m {
							mut mp := m[tempstr].map() ?
							add_val(mut mp, key[index + 1..], value) ?
							m[tempstr] = mp
						} else {
							mut mp := map[string]Value{}
							add_val(mut mp, key[index + 1..], value) ?
							m[tempstr] = mp
						}
						done = true
						break
					}
				}
				`"` {
					if !lit_str {
						str = !str
						continue
					}
				}
				`\'` {
					if !str {
						lit_str = !lit_str
						continue
					}
				}
				else {}
			}
			tempstr += char.ascii_str()
		}
		if !done {
			dump(tempstr)
			if tempstr in m {
				mut mp := m[tempstr].map() ?
				add_val(mut mp, key, value) ?
				m[tempstr] = mp
			} else {
				mut mp := map[string]Value{}
				add_val(mut mp, key, value) ?
				m[tempstr] = mp
			}
		}
	} else {
		if (key.starts_with('"') && key.ends_with('"'))
			|| (key.starts_with("'") && key.ends_with("'")) {
			m[key[1..key.len - 1]] = value
		} else {
			m[key] = value
		}
	}
}

// this makes printing values not look ugly
fn (v Value) str() string {
	match v {
		bool {
			return v.str()
		}
		f64 {
			return v.str()
		}
		[]Value {
			return v.str()
		}
		map[string]Value {
			return v.str()
		}
		string {
			return "'" + v.str() + "'"
		}
		int {
			return v.str()
		}
		time.Time {
			return v.str()
		}
	}
}

fn (v Value) int() ?int {
	if v is int {
		return v
	}
	return error('Value is not int')
}

fn (v Value) f64() ?f64 {
	if v is f64 {
		return v
	}
	return error('Value is not f64')
}

fn (v Value) bool() ?bool {
	if v is bool {
		return v
	}
	return error('Value is not bool')
}

fn (v Value) string() ?string {
	if v is string {
		return v
	}
	return error('Value is not string')
}

fn (v Value) map() ?map[string]Value {
	if v is map[string]Value {
		return v
	}
	return error('Value is not map')
}

fn (v Value) array() ?[]Value {
	if v is []Value {
		return v
	}
	return error('Value is not array')
}
